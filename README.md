# Laundry folding planner

## Use

How to use this project

## Dependencies

1. `python2.7`
    1. `shapely`
    2. `Tkinter`
2. ABB robot arm
3. Webcam

## Implementation

For details of the model and search algorithm, see `folding_planner/README.md`.

## Paper

For the paper on this project, see https://www.overleaf.com/read/csxrygpzhcch. 

## TODO

1. Make the robot accept fold instructions from planner
2. Make the planner consider key folding options, such as half-of-the-height folds.

## Credits

Devin Balkcom
Evan Honnold
Josiah Putman
Dartmouth Robotics Lab 18W

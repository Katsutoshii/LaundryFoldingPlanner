def binarySearch(alist, item):
    if not alist:
        return None

    midpoint = len(alist) // 2

    # if equal
    if alist[midpoint] == item:
        return midpoint

    # if less than
    if item < alist[midpoint]:
        i = binarySearch(alist[:midpoint], item)
        return i if i is not None else None

    # if greater than
    i = binarySearch(alist[midpoint+1:], item)
    return (midpoint + 1 + i) if i is not None else None


testlist = [0, 1, 2, 8, 13, 17, 19, 32, 42, ]
print(binarySearch(testlist, 3))
print(binarySearch(testlist, 17))

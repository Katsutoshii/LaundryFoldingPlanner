'''
File: test_polygon.py
Project: folding_planner
File Created: Tuesday, 13th February 2018 2:34:31 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 13th February 2018 2:34:59 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

from time import sleep
from model.polygon import Polygon
from model.polygon_graphics import run_polygon_graphics
from test_shapes import T_SHIRT


def test_polygon():

    canvas1, canvas2 = run_polygon_graphics("Testing polygon")

    canvas1.create_text(50, 20, text="stack height = 1")

    polygon = Polygon(T_SHIRT)
    polygon.draw(canvas1)
    sleep(50)


print "running test polygon"
test_polygon()

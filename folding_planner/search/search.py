'''
File: search.py
Project: search
File Created: Tuesday, 27th February 2018 6:23:40 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 27th February 2018 6:23:42 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

class SearchNode(object):
    """Class for a Search Node
    """

    def __init__(self, state, parent=None, transition_cost=0):
        """Constructor for AstarNode
        
        Arguments:
            state {[type]} -- [description]
            heuristic {[type]} -- [description]
        
        Keyword Arguments:
            parent {[type]} -- [description] (default: {None})
            transition_cost {[type]} -- [description] (default: {0})
        """

        self.state = state
        self.parent = parent
        self.transition_cost = transition_cost
        

def backchain(node):
    """[summary]
    
    Arguments:
        node {[SearchNode]} -- [node to backtrace from]
    
    Returns:
        [type] -- [result]
    """

    result = []
    current = node
    while current:
        result.append(current.state)
        current = current.parent

    result.reverse()
    return result

def select_max_successor(search_problem, successors, scoring_fn, visited):
    """Method to return the maximizing successor state of a given node
    
    Arguments:
        search_problem {SearchProblem} -- The search problem
        node {SearchNode} -- node to check successors of
        scoring_fn {method} -- funciton to evaluate a state
    
    Returns:
        SearchNode -- a node containing the maximum successor state with the current 
            node as its parent
    """

    max_score = -float("inf")
    max_state = None

    # pick the maximum out of the successor states
    print "Getting successors"
    for state in successors:
        # ignore visited states
        if state in visited:
            continue

        score = scoring_fn(search_problem, state)

        if score > max_score:
            print "New max found with score", score, "for state", state
            max_score = score
            max_state = state

    return max_state

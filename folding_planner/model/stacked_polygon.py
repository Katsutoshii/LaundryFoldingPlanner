'''
File: stacked_polygon.py
Project: folding_planner
File Created: Wednesday, 24th January 2018 3:23:07 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Friday, 26th January 2018 4:22:57 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

from time import sleep
from math import pi

from shapely import geometry as geo
from shapely.affinity import translate, rotate
# pylint: disable=E0401
from libs.math_lib import add2D, sub2D, will_fold

from polygon import Polygon
from fold import Fold


class StackedPolygon:
    """A stacked polygon contains one base polygon and any number of stacks, which are other 
    stacked polygons
    """

    def __init__(self, polygon=Polygon([]), depth=0, fold_line_center=0, target=None):
        """Constructor for a stacked polygon

        Keyword Arguments:
            polygon {Polygon} -- the base of this stacked polygon (default: {None})
        """

        self.item = 0
        self.depth = depth
        self.layers = [
            [polygon],
        ]
        self.fold_line_center = fold_line_center
        self.height = 1
        self.current_fold = None
        self.silhouette = polygon

        self.target = target

    def fold(self):

        # add a new layer to the stack
        new_layers = []

        for layer in reversed(self.layers):
            new_layer, new_layer_folded = self.fold_layer(layer)

            # add these layers to the ends of the list of layers
            new_layers.insert(0, new_layer)
            new_layers.append(new_layer_folded)

        # update variables
        self.layers = new_layers
        self.height = len(self.layers)
        self.silhouette = self.get_silhouette()

        # update the target
        starget = geo.Polygon(self.target.vertices)
        test_point = starget.representative_point()

        if will_fold((test_point.x, test_point.y), self.fold_line_center):
            self.target.flip(self.fold_line_center)

    def fold_layer(self, layer):
        if not layer:
            return [], []

        # fold each polygon in this layer
        for polygon in layer:
            # split the polygon
            new_polygons1, new_polygons2 = polygon.split(self.fold_line_center)

            # everything in new_polygons2 must be flipped
            for new_polygon in new_polygons2:
                new_polygon.flip(self.fold_line_center)

        return new_polygons1, new_polygons2

    def draw(self, canvas, outline='black', fill='gray', width=2):
        """Method to draw a stacked polygon

        Arguments:
            canvas {Jkinter.canvas} -- canvas to draw on
        """

        if self.depth == 1:
            fill = 'black'

        # draw the target
        if self.target is not None:
            self.target.draw(canvas, outline=outline, fill="blue", width=width)

        # draw the base
        for layer in self.layers:
            for polygon in layer:
                polygon.draw(canvas, outline=outline, fill=fill, width=width)

    def align_and_fold(self, fold):
        """Method to make a fold on a stacked polygon

        Arguments:
            point_a {point} -- point of one end of the fold line
            point_b {point} -- point of the other end of the fold line
            side {bool} -- which side to flip
        """
        self.update_fold(fold)

        # first align the polygon
        self.align()

        self.fold()

    def align_and_draw_fold(self, fold, canvas):
        """Method to align, perform and draw a fold

        Arguments:
            point_a {tuple} -- point A of the line to fold across
            point_b {tuple} -- point B of the line to fold across
            side {boolean} -- which side to fold to
            canvas {Pkinter.canvas} -- canvas to draw on
        """

        self.erase(canvas)

        self.update_fold(fold)

        self.align()
        
        self.draw_fold(fold, canvas)

    def area_diff(self, canvas=None):
        """Method to compare this stacked polygon's silhouette to a target polygon

        Arguments:
            target {Polygon} -- target_polygon
        """
        # create shapely objects for each polygon
        spolygon = geo.Polygon(self.silhouette.vertices)
        starget = geo.Polygon(self.target.vertices)

        # translate polygons so they have the same centroid
        # pylint: disable=E1101
        # centroid_dx = spolygon.centroid.x - starget.centroid.x
        # centroid_dy = spolygon.centroid.y - starget.centroid.y

        # starget = translate(starget, xoff=centroid_dx, yoff=centroid_dy)

        # draw for debugging
        if canvas is not None:
            target = Polygon(list(starget.exterior.coords))
            target.draw(canvas)

            polygon = Polygon(list(spolygon.exterior.coords))
            polygon.draw(canvas)

            target.erase(canvas)
            polygon.erase(canvas)

        # intersect the two
        intersection = spolygon.intersection(starget)
        union = spolygon.union(starget)

        # this way folds that mess up the target shape get discounted
        if intersection.area < starget.area:
            return float('inf')

        return union.area - intersection.area

    def get_silhouette(self):
        """Method to get a silhouette of this stacked polygon
        """
        silhouette = geo.Polygon([])

        # union all polygons together
        for layer in self.layers:
            for polygon in layer:
                spolygon = geo.Polygon(polygon.vertices)

                if not spolygon.is_valid:
                    print "Shapely polygon was null from polygon "
                else:
                    silhouette = silhouette.union(spolygon)

        if isinstance(silhouette, geo.Polygon):
            return Polygon(list(silhouette.exterior.coords))
        elif isinstance(silhouette, geo.GeometryCollection):
            for geom in silhouette:
                if isinstance(geom, geo.Polygon):
                    return Polygon(list(geom.exterior.coords))

        return Polygon([])

    def get_max_dimensions(self):
        """Method to return the max dimensions of this stacked polygon

        Returns:
            tuple -- min_x, min_y, max_x, max_y
        """

        return self.silhouette.get_max_dimensions()

    def update_fold(self, fold):
        self.current_fold = fold

    def draw_fold(self, fold, canvas):
        fold_line = canvas.create_line(self.fold_line_center, 0,
                                       self.fold_line_center, 250, width=4, dash=(3, 2), fill="red")

        self.draw(canvas)

        x = raw_input("fold")

        self.erase(canvas)

        self.fold()

        self.draw(canvas)

        x = raw_input("erase")

        self.erase(canvas)
        canvas.delete(fold_line)

    def align(self):
        """Aligns a stacked polygon for the next fold

        Arguments:
            fold {Fold} -- the fold to be aligned with
        """

        min_y = float("inf")

        for layer in self.layers:
            for i, polygon in enumerate(layer):

                spolygon = geo.Polygon(polygon.vertices)

                rotated = rotate(spolygon, self.current_fold.angle,
                                 use_radians=False, origin=self.current_fold.point)
                translated = translate(rotated, xoff=self.current_fold.dx)

                # find min y
                for vertex in translated.exterior.coords:
                    if (vertex[1] < min_y):
                        min_y = vertex[1]

                layer[i] = Polygon(list(translated.exterior.coords))

        # shift the entire stack over by the min
        for layer in self.layers:
            for i, polygon in enumerate(layer):

                spolygon = geo.Polygon(polygon.vertices)
                translated = translate(spolygon, xoff=0.0, yoff=-min_y + 2)
                # +2 to make the image cleaner
                layer[i] = Polygon(list(translated.exterior.coords))

        # move the target along with the shape

        starget = geo.Polygon(self.target.vertices)
        starget = rotate(starget, self.current_fold.angle,
                         use_radians=False, origin=self.current_fold.point)

        starget = translate(
            starget, xoff=self.current_fold.dx, yoff=-min_y + 2)

        self.target = Polygon(list(starget.exterior.coords))

    def redraw(self, canvas, outline='black', fill='gray', width=2):
        """Method to redraw a stacked polygon on a canvas

        Arguments:
            canvas {Pkinter.canvas} -- Canvas to redraw on
        """

        self.erase(canvas)
        self.draw(canvas, outline=outline, fill=fill, width=width)

    def erase(self, canvas):
        """Method to erase a polygon

        Arguments:
            canvas {Pkinter.canvas} -- canvas to be erased from
        """
        # erase all polygons
        for layer in self.layers:
            for polygon in layer:
                polygon.erase(canvas)

        if self.target is not None:
            self.target.erase(canvas)

    def __str__(self):
        string = "StackedPolygon {\n"

        for i, layer in enumerate(self.layers):
            string += "    layer " + str(i) + ":\n"

            for polygon in layer:
                string += "    " + str(polygon) + "\n"

        return string + "}\n"

    def __repr__(self):
        if self.current_fold is None:
            return "start"

        return str(self.current_fold)

'''
File: folding_problem.py
Project: folding_planner
File Created: Tuesday, 23rd January 2018 3:44:38 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 23rd January 2018 5:43:32 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

from copy import deepcopy
from time import sleep
from math import atan2, pi

from shapely import geometry as geo
from shapely.affinity import translate, rotate

from polygon import Polygon
from fold import Fold
from search.search_problem import SearchProblem
from libs.math_lib import avg2D


# pylint: disable=R0902
class FoldingProblem(SearchProblem):
    """Class to define the folding problem
    Extends search problem
    """
    # pylint: disable=R0913
    def __init__(self, base, allowed_error=0.2, max_g_dif=0.2, canvas1=None, 
                 canvas2=None, draw_time=0.5, folding_line_center=0):
        """Constructor for a folding search problem
        
        Arguments:
            base {StackedPolygon} -- a stacked polgon that is the base of the cloth
            target {Polygon} -- a polgon that the item should be folded into
        """
        super(FoldingProblem, self).__init__()
        self.name = "Folding problem"
        self.base = base
        self.allowed_error = allowed_error
        self.min_score = -1000
        self.max_g_dif = max_g_dif

        self.canvas1 = canvas1
        self.canvas2 = canvas2
        self.draw_time = draw_time
        self.prev_drawn_state = None
        
        self.folding_line_center = folding_line_center

    def get_start_state(self):
        """Method to get a start state for the search problem
        
        Returns:
            StackedPolygon -- a stacked polygon to start from
        """

        return self.base

    @staticmethod
    def insert_folding_vertices(state):
        """Method to create a polygon that has all of the required vertices for the fold inserted
        """
        
        polygon = state.silhouette

        num_vertices = len(polygon.vertices)
        
        polygon_with_halfways = Polygon([]) # to hold a poylgon that has all vertices that
                                            # we might want to fold

        min_x, min_y, max_x, max_y = state.get_max_dimensions()
        half_x = (min_x + max_x) / 2
        half_y = (min_y + max_y) / 2

        # add all vertices and halfway points
        for i in range(num_vertices - 1):
            polygon_with_halfways.vertices.append(polygon.vertices[i])
            halfway_point = avg2D(polygon.vertices[i], polygon.vertices[i + 1])
            polygon_with_halfways.vertices.append(halfway_point)

            # if there is an intersection between the line segment and one of the half 
            # way lines insert it
            p1 = polygon.vertices[i]
            p2 = polygon.vertices[i + 1]

            # first find which point is farther to the right
            if p1[0] >= p2[0]:

                max_point_x = p1
                min_point_x = p2

            else:
                max_point_x = p2
                min_point_x = p1
                
            # then add the point if it intersects
            if (min_point_x[0] < half_x < max_point_x[0]):
                # calculate the y position
                y_pos = min_point_x[1] + \
                    ((max_point_x[1] - min_point_x[1]) / (max_point_x[0] - min_point_x[0])) \
                    * (half_x - min_point_x[0])

                print "ADDED A HALFWAY FOR WIDTH at", (half_x, y_pos)
                polygon_with_halfways.vertices.append((half_x, y_pos))

            # first find which point is higher
            if p1[1] >= p2[1]:

                max_point_y = p1
                min_point_y = p2

            else:
                max_point_y = p2
                min_point_y = p1

            # makes sure it is in the window and doesn't divide by 0
            if (min_point_y[1] < half_y < max_point_y[1]):
                m = float((max_point_y[0] - min_point_y[0])) / (max_point_y[1] - min_point_y[1])

                # calculate the x position
                x_pos = min_point_y[0] + m * (half_y - min_point_y[1])

                print "ADDED A HALFWAY FOR HEIGHT at", (x_pos, half_y), "from line segment", \
                    min_point_y, max_point_y
                polygon_with_halfways.vertices.append((x_pos, half_y))
        
        # add in the last one
        polygon_with_halfways.vertices.append(polygon.vertices[num_vertices - 1])

        return polygon_with_halfways

    def get_successors(self, state):
        successors = []    # a list of successor states
        tested_folds = set()

        polygon = self.insert_folding_vertices(state)

        # make all folds from each possible vertex
        for i1, vertex1 in enumerate(polygon.vertices):
            for i2, vertex2 in enumerate(polygon.vertices):
                
                # ignore folds that go through the target
                if state.target.line_intersects(vertex1, vertex2):
                    continue

                # calculate parameters for this fold
                angle = 90 + atan2(vertex2[1] - vertex1[1], vertex2[0] - vertex1[0]) * 180 / pi
                dx = self.folding_line_center - vertex1[0] 

                fold = Fold(vertex1, angle, dx)

                # skip already done folds and adjacent vertices
                if fold in tested_folds or len(polygon.vertices) < abs(i1 - i2) < 2:
                    continue

                print "    checking:", fold, "on", state
                
                tested_folds.add(fold)
                # copy the polygon and align it
                new_state = deepcopy(state)
                new_state.update_fold(fold)
                new_state.align()

                # don't make folds if the gravity balance is off
                if self.is_valid_fold(new_state):

                    # if we were given a canvas, draw the fold
                    if self.canvas2 is not None:
                        new_state.draw_fold(fold, canvas=self.canvas2)
                        sleep(0.1)
                        new_state.erase(canvas=self.canvas2)

                    else:
                        new_state.fold()
                        
                    successors.append(new_state)

        return successors

    def is_valid_fold(self, state):
        
        # first check to make sure that all layers have the correct gravity proportion
        # this is to prevent the AI from trying folds that might make the shirt slide off
        for layer in state.layers:

            # if this is an empty layer
            if not layer:
                continue

            for polygon in layer:
                weight_proportion = polygon.weight_proportion(self.folding_line_center)

                if abs(weight_proportion - 0.5) >= self.max_g_dif:
                    return False

        return True

    def accuracy_heuristic(self, state):
        # rotate the target by the absolute angle of the current stacked polygon
        area_diff = state.area_diff(canvas=self.canvas2)

        # calculates the accuracy of the a current state
        return -area_diff if area_diff > 0 else -float("inf")

    def is_goal(self, state):
        """Method to determine if a state is the goal
        
        Arguments:
            state {StackedPolygon} -- the current state in the folding problem
        
        Returns:
            True if the accuracy is within the alloted error
        """

        return self.accuracy_heuristic(state) >= self.min_score

    # pylint: disable=W0613, R0201
    def get_transition_cost(self, state1, state2):
        # all folds just cost 1 for now
        return 1

     # pylint: disable=R0201
    def problem_is_valid(self):
        return True

    def draw_state(self, state):
        if self.canvas1 is None:
            return

        # erase the old state
        if self.prev_drawn_state is not None:
            self.prev_drawn_state.erase(self.canvas1)

        state.draw(self.canvas1)
        sleep(self.draw_time)
        # hold on to this state so we can erase it
        self.prev_drawn_state = state
        
    def __str__(self):
        return self.name

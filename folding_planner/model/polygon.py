'''
File: polygon.py
Project: folding_planner
File Created: Wednesday, 24th January 2018 3:34:27 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Wednesday, 24th January 2018 3:34:32 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''
from shapely import geometry as geo
# from shapely.ops import triangulate
# from shapely.topology import TopologicalError

# from libs.util_lib import dlog
# pylint: disable=E0401
from libs.math_lib import split_polygon_line, \
    will_fold, reflect_point_over_vertical


class Polygon:
    def __init__(self, vertices, origin=(0, 0)):
        self.vertices = vertices
        self.origin = origin
        self.item_id = -1

    def split(self, fold_line_center):
        line = geo.LineString(((fold_line_center, 0), (fold_line_center, 1000)))

        polygon = geo.Polygon(self.vertices)
        collection = split_polygon_line(polygon, line)

        new_polygons1 = []
        new_polygons2 = []

        # check all resulting reometry from the split
        for geometry in collection:
            if isinstance(geometry, geo.Polygon):
                test_point = geometry.representative_point()

                if will_fold((test_point.x, test_point.y), fold_line_center):
                    # print "    will fold:", geometry
                    new_polygons2.append(
                        Polygon(list(geometry.exterior.coords)))
                else:
                    # print "    will not fold:", geometry
                    new_polygons1.append(
                        Polygon(list(geometry.exterior.coords)))

        return new_polygons1, new_polygons2

    def flip(self, fold_line_center):
        new_vertices = []

        for vertex in self.vertices:
            new_vertices.append(reflect_point_over_vertical(
                vertex, fold_line_center))

        self.vertices = new_vertices

    def line_intersects(self, v1, v2):
        line = geo.LineString((v1, v2))

        polygon = geo.Polygon(self.vertices)

        print "does line intersect with poylgon?"
        collection = split_polygon_line(polygon, line)

        count_polygon = 0

        # check all resulting reometry from the split
        for geometry in collection:
            if isinstance(geometry, geo.Polygon):
                print "     polygon found in interseciton split!"
                count_polygon += 1

        return count_polygon > 1

    def draw(self, canvas=None, outline='black', fill='gray', width=2):
        """Method to draw a polygon

        Arguments:
            canvas {Jkinter.canvas} -- canvas to draw on
        """
        if canvas is None:
            print("Canvas is none")
            return

        coords = []
        for vertex in self.vertices:
            coords.append(vertex[0] + self.origin[0])
            coords.append(vertex[1] + self.origin[1])
        if coords:
            if fill == 'gray':
                fill = fill + '50'
                self.item_id = canvas.create_polygon(coords, outline=outline,
                                                 fill=fill, stipple=fill, width=width)
            else:
                self.item_id = canvas.create_polygon(coords, outline=outline,
                                                 fill=fill, width=width)
        else:
            print("Tried drawing empty poylgon")

    def erase(self, canvas):
        """Method to erase a polygon from the canvas

        Arguments:
            canvas {Pkinter.canvas} -- canvas to be erased from
        """

        if self.item_id >= 0:
            canvas.delete(self.item_id)

    def weight_proportion(self, folding_line_center):
        """Method to calculate the proportion of weight on each side
        of the folding rod
        
        Arguments:
            vertex1 {tuple} -- vertex1 of the fold line
            vertex2 {tuple} -- vertex2 of the fold line
        
        Returns:
            float -- (total_left) / (total)
        """

        line = geo.LineString(((folding_line_center, 0), (folding_line_center, 1000)))
        polygon = geo.Polygon(self.vertices)

        collection = split_polygon_line(polygon, line)
        
        # if this is just one polygon, instantly fail, return a 1
        if not isinstance(collection, geo.GeometryCollection):
            return 1

        # check all resulting geometry from the split
        total_area = 0
        total_area_left = 0

        for geometry in collection:
            if geometry.representative_point().x < folding_line_center:
                total_area_left += geometry.area

            total_area += geometry.area

        # otherwise return the ratio
        return total_area_left / total_area

    
    def get_max_dimensions(self):
        """Method to get the max height from a polygon

        Returns min_x, min_y, max_x, max_y
        """
        min_x, min_y = float('inf'), float('inf')
        max_x, max_y = -float('inf'), -float('inf')

        # loop over all points and find the maximum
        for point in self.vertices:
            if point[0] > max_x:
                max_x = point[0]
            
            if point[0] < min_x:
                min_x = point[0]

            if point[1] > max_y:
                max_y = point[1]

            if point[1] < min_y:
                min_y = point[1]
            
        return min_x, min_y, max_x, max_y

    def __str__(self):
        return "Polygon {" + str(self.vertices) + "}"

    def __repr__(self):
        return str(self)


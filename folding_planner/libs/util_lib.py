'''
File: util_lib.py
Project: libs
File Created: Tuesday, 13th February 2018 1:00:01 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 13th February 2018 1:00:19 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''
debug = False

def dlog(string):
    if debug:
        print string

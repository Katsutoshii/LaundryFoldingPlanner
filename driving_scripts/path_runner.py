'''
File: path_runner.py
Project: driving_scripts
File Created: Thursday, 5th April 2018 3:32:44 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Thursday, 5th April 2018 3:32:48 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''
from helpers import get_robot_controller
from paths import T_SHIRT_PATH

def lift_sequence(robot):
    """Method to run the lifting up sequence
    Assumes that the folding rod is under the cloth and
    that the cloth is placed correctly
    
    Arguments:
        robot {robot} -- the robot to run the sequence
    """

    b1 = [[-275.36, -272.72, 331.86], [0.045, 0.868, -0.492, 0.051]]
    # ruler prepared to slide under the shirt:
    b2 = [[-275.4, -272.7, 131.9], [0.0, 0.919, 0.394, 0.0]]
    # ruler after sliding under the shirt:
    b3 = [[102.59, -272.69, 131.9], [0, 0.919, 0.394, 0]]
    # after lifting the shirt up:
    b4 = [[124.6, -272.71, 609.37], [0, 0.919, 0.394, 0]]

    robot.set_cartesian(b1)
    robot.set_cartesian(b2)
    robot.set_cartesian(b3)
    robot.set_cartesian(b4)

# pylint: disable=W0613
def fold_sequence(robot, fold):
    """Method to place down the cloth given a next fold
    Assumes that the robot is already lifted and that a gravity fold has been completed
    
    Arguments:
        robot {robot} -- the robot to run the sequence
        fold {Fold} -- the fold to be run next
    """

    pass

def main():
    print("beginning script")
    (robot, succeeded) = get_robot_controller()

    if not succeeded:
        return

    for fold in T_SHIRT_PATH[1:]:
        lift_sequence(robot)
        fold_sequence(robot, fold)

if __name__ == '__main__' and __package__ is None:
    __package__ = 'driving_scripts.path_runner'
    main()
